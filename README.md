# Install
https://www.11ty.dev/docs/getting-started/

# Start

npx @11ty/eleventy

# Dev

npx @11ty/eleventy --serve
